var gulp    = require('gulp'),
    glob    = require('glob'),
    config  = require('./config');

var BUILD_TASKS_PATH = './build_tasks/';

// load all build tasks
glob.sync('*', {cwd: BUILD_TASKS_PATH}).forEach(function(option) { require(BUILD_TASKS_PATH + option)(gulp, config); });


// Custom Tasks

gulp.task('dev', function(){
    gulp.start('clean:dist', 'scripts:dev', 'concat:dev', 'cssmin:vendor', 'cssmin:app');
});