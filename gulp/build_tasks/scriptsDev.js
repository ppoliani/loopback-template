var webpack = require('gulp-webpack'),
    concat  = require('gulp-concat');

// config
var webpackOptions = {

    output: {
        filename: 'bundle.js'
    },

    externals: {
        'angular': 'angular'
    },

    devtool: '#source-map'
};


module.exports = function(gulp, config){
    gulp.task('scripts:dev', function(){
        return gulp.src(config.appDir + '/core/app.js')
            .pipe(webpack(webpackOptions))
            .pipe(gulp.dest(config.distDir + '/'));
    });

    gulp.task('concat:dev', function(){
        return gulp.src(config.bundles.js.vendor)
            .pipe(concat('vendor.js'))
            .pipe(gulp.dest(config.distDir + '/'));
    });
};