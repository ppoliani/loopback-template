var minifyCSS = require('gulp-minify-css'),
    concat  = require('gulp-concat');

module.exports = function(gulp, config){
    gulp.task('cssmin:vendor', function(){
        return gulp.src(config.bundles.css.vendor)
            .pipe(concat("vendor.min.css"))
            .pipe(minifyCSS({keepBreaks:true}))
            .pipe(gulp.dest(config.distDir));
    });

    gulp.task('cssmin:app', function(){
        return gulp.src(config.bundles.css.app)
            .pipe(concat("app.min.css"))
            .pipe(minifyCSS({keepBreaks:true}))
            .pipe(gulp.dest(config.distDir));
    });
};