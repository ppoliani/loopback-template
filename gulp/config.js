﻿var distDir = '../client/dist',
    libsDir = '../client/third-party',
    cssDir ='../client/assets/css',
    appDir = '../client/app';

var bundles = {
    js: {
        vendor: [
            libsDir + '/angular/angular.js',
            libsDir + '/jquery/jquery.min.js',
            libsDir + '/bootstrap/dist/js/bootstrap.min.js',
            libsDir + '/prettyphoto/js/prettyphoto.js',
            libsDir + '/owl-carousel/js/owl.carousel.min.js',
            libsDir + '/helper-plugins.js',
            libsDir + '/bootstrap/dist/js/bootstrap.js',
            libsDir + '/jquery-waypoints/waypoints.js',
        ],

        app: [

        ]
    },

    css: {
        vendor: [
            libsDir + '/bootstrap/dist/css/bootstrap.min.css',
            libsDir + '/prettyphoto/css/prettyPhoto.css',
            libsDir + '/owl-carousel/css/owl.carousel.css',
            libsDir + '/owl-carousel/css/owl.theme.css',
        ],

        app: [
            cssDir + '/style.css',
            cssDir + '/color.css',
            cssDir + '/ie.css'
        ]
    }
};

var copyFiles = {
    foundationFonts: '<%= cssDir %>/foundation-icons/fonts/**'
};


module.exports =  {
    bundles: bundles,
    copyFiles: copyFiles,
    distDir: distDir,
    libsDir: libsDir,
    cssDir: cssDir,
    appDir: appDir
};