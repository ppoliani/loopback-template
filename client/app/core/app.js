/**
 * App routes
 */
var app = (function(){
    'use strict';

    // region Deps

    var
        angular = require('angular'),
        routes = require('../config/routes'),
        controllers = require('./controllers'),
        services = require('./services'),
        values = require('./values'),
        constants = require('./constants'),
        filters = require('./filters'),
        directives = require('./directives'),
        init = require('./init');

    // endregion

    // region Private Fields

    var mainModule = angular.module('cpf.core', [
            'cpf.services',
            'cpf.controllers',
            'cpf.filters',
            'cpf.models',
            'cpf.directives'
        ]),
        servicesModule = angular.module('cpf.services', []),
        controllersModule = angular.module('cpf.controllers', []),
        filtersModule = angular.module('cpf.filters', []),
        modelsModule = angular.module('cpf.models', []),
        directivesModule = angular.module('cpf.directives', []);

    // endregion

    // region Register all Controllers

    controllers.forEach(function(controller){
        controllersModule.controller(controller.name, controller.ctrl);
    });

    // endregion

    // region Register All Services

    services.concat(values).concat(constants).forEach(function(service){
        servicesModule[service.type](service.name, service.service);
    });

    // endregion

    // region Register All Filters

    filters.forEach(function(service){
        filtersModule[service.type](service.name, service.filter);
    });

    // endregion

    // region Register All Directives

    directives.forEach(function(directive){
        directivesModule.directive(directive.name, directive.directive);
    });

    // endregion

    // region Config Phase

    routes.configure(mainModule);

    // endregion

    // region Run Phase

    mainModule.run(init);

    // endregion

    // region Public API

    return mainModule;

    // endregion

})();

// region CommonJS

module.exports = app;

// endregion